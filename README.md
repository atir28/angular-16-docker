# Angular16Docker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

# Starting docker
- On root path `dokcer-compose up -d`. It will create dcoker image.
- Check the `expose port` and use it in browser for testing. 


## Docker configuration
 - nginx configuration on file `nginx.conf`
 - nginx middle configuration on file `nginx.middle.conf`
 - Docker build file `Dockerfile`.
 - Docker compose file `docker-compose.yml`.

